#include <gst/gst.h>

#define NB_BRANCHES 5
#define NB_BUFFERS 100

static GMainLoop *loop = NULL;
static GHashTable *branches = NULL;

static gboolean
check_eos_sink(GstElement *element, GstPad *pad, gpointer user_data)
{
    gboolean *called = (gboolean *)user_data;

    *called = TRUE;
    return GST_PAD_IS_EOS(pad);
}

typedef gboolean (*ElementForeachPadFunc)(GstElement *element,
                                          GstPad *pad,
                                          gpointer user_data);
static gboolean
element_foreach_sink_pad(GstElement *element, ElementForeachPadFunc func, gpointer user_data)
{
    GList *l;
    gboolean res = FALSE;

    GST_OBJECT_LOCK(element);
    for (l = element->sinkpads; l; l = g_list_next(l))
    {
        GstPad *pad = (GstPad *)l->data;

        res = func(element, pad, user_data);
        if (!res)
            goto done;
    }

done:
    GST_OBJECT_UNLOCK(element);
    return res;
}

static void
check_eos(GstPipeline *pipeline)
{
    GstIterator *it;
    GValue item = G_VALUE_INIT;
    gboolean done = FALSE;
    gboolean all_sink_eos = TRUE;

    it = gst_bin_iterate_recurse(GST_BIN(pipeline));
    while (!done)
    {
        switch (gst_iterator_next(it, &item))
        {
        case GST_ITERATOR_OK:
        {
            GstElement *child = GST_ELEMENT_CAST(g_value_get_object(&item));

            if (GST_OBJECT_FLAG_IS_SET(child, GST_ELEMENT_FLAG_SINK))
            {
                gboolean called = FALSE, elt_eos;

                elt_eos = element_foreach_sink_pad(child, check_eos_sink, &called);
                if (called && !elt_eos)
                {
                    all_sink_eos = FALSE;
                    done = TRUE;
                }
            }

            g_value_reset(&item);
        }
        break;
        case GST_ITERATOR_RESYNC:
            gst_iterator_resync(it);
            break;
        case GST_ITERATOR_ERROR:
        case GST_ITERATOR_DONE:
            done = TRUE;
            break;
        }
    }
    g_value_unset(&item);
    gst_iterator_free(it);

    if (all_sink_eos)
    {
        g_print("Remaining sinks are already EOS; stopping\n");
        g_main_loop_quit(loop);
    }
}

static gboolean
handle_message(GstBus *bus, GstMessage *message, gpointer user_data)
{
    GstPipeline *pipeline = GST_PIPELINE_CAST(user_data);

    switch (GST_MESSAGE_TYPE(message))
    {
    case GST_MESSAGE_ERROR:
    {
        GError *err = NULL;
        gchar *dbg_info = NULL;

        gst_message_parse_error(message, &err, &dbg_info);
        g_print("Error: %s (%s)", err->message, dbg_info);

        g_error_free(err);
        g_free(dbg_info);
        g_main_loop_quit(loop);
    }
    break;

    case GST_MESSAGE_EOS:
        g_print("got EOS!\n");
        g_main_loop_quit(loop);
        break;

    case GST_MESSAGE_ELEMENT:
    {
        if (gst_message_has_name(message, "x-handle-error:message-error") ||
            gst_message_has_name(message, "x-handle-error:chain-error"))
        {
            GstElement *source, *bin;
            const gchar *name;
            gchar *str;

            source = GST_ELEMENT_CAST(GST_MESSAGE_SRC(message));
            name = GST_MESSAGE_SRC_NAME(message);
            str = gst_structure_to_string(gst_message_get_structure(message));

            g_print("handle error from %s (%s)\n", name, str);
            bin = GST_ELEMENT_CAST(g_hash_table_lookup(branches, source));
            if (bin)
            {
                g_print("Remove branch %s\n", GST_OBJECT_NAME(bin));
                gst_element_set_state(bin, GST_STATE_NULL);
                g_assert(gst_bin_remove(GST_BIN(pipeline), bin));
                g_hash_table_remove(branches, source);

                check_eos(pipeline);
            }
        }
    }
    break;

    default:
        break;
    }
    return TRUE;
}

static GstElement *create_pipeline(void)
{
    GstElement *pipeline;
    guint i;

    pipeline = gst_pipeline_new(NULL);

    for (i = 0; i < NB_BRANCHES; i++)
    {
        GstElement *src, *identity, *fakesink, *bin, *h;

        src = gst_element_factory_make("audiotestsrc", NULL);
        g_assert(src);
        g_object_set(src, "num-buffers", NB_BUFFERS, NULL);

        identity = gst_element_factory_make("identity", NULL);
        g_assert(identity);
        if (i != 0)
        {
            // First branch never fails
            gint32 fail;

            fail = g_random_int_range(0, NB_BUFFERS);
            g_print("branch %d will fail after %d buffers\n", i, fail);
            g_object_set(identity, "error-after", fail, NULL);
        }

        fakesink = gst_element_factory_make("fakesink", NULL);
        g_assert(fakesink);

        // Wrap element that may fail
        h = gst_element_factory_make("handleerrors", NULL);
        g_assert(h);

        g_assert(gst_bin_add(GST_BIN(h), identity));

        // Encapsulate the branch in a bin
        bin = gst_element_factory_make("bin", NULL);
        g_assert(bin);

        gst_bin_add_many(GST_BIN(bin), src, h, fakesink, NULL);
        g_hash_table_insert(branches, gst_object_ref(h), gst_object_ref(bin));

        gst_element_link_many (src, identity, fakesink, NULL);

        g_assert(gst_bin_add(GST_BIN(pipeline), bin));
    }

    return pipeline;
}

int main(int argc, char **argv)
{
    gst_init(&argc, &argv);

    {
        g_autoptr(GstBus) bus = NULL;
        g_autoptr(GstElement) pipeline = NULL;

        loop = g_main_loop_new(NULL, FALSE);
        branches = g_hash_table_new_full(NULL, NULL, gst_object_unref, gst_object_unref);
        pipeline = create_pipeline();

        bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
        gst_bus_add_watch(bus, handle_message, pipeline);

        g_print("setting pipeline PLAYING\n");
        gst_element_set_state(pipeline, GST_STATE_PLAYING);

        g_main_loop_run(loop);

        g_print("setting pipeline NULL\n");
        gst_element_set_state(pipeline, GST_STATE_NULL);
        gst_bus_remove_watch(bus);
        g_hash_table_unref(branches);
    }

    gst_deinit();
    return 0;
}
