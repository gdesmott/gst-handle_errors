#ifndef __GST_HANDLE_ERRORS_H__
#define __GST_HANDLE_ERRORS_H__

#include <gst/gst.h>

G_BEGIN_DECLS

#define GST_TYPE_HANDLE_ERRORS (gst_handle_errors_get_type())
G_DECLARE_FINAL_TYPE(GstHandleErrors, gst_handle_errors, GST, HANDLE_ERRORS, GstBin)

G_END_DECLS

#endif