#include "gsthandleerrors.h"

GST_DEBUG_CATEGORY_STATIC(gst_handle_errors_debug);
#define GST_CAT_DEFAULT gst_handle_errors_debug

#define gst_handle_errors_parent_class parent_class

static GQuark override_quark;

struct _GstHandleErrors
{
    GstBin parent;
};

G_DEFINE_TYPE_WITH_CODE(GstHandleErrors, gst_handle_errors,
                        GST_TYPE_BIN, GST_DEBUG_CATEGORY_INIT(gst_handle_errors_debug, "handleerrors", 0, "handle-errors");
                        override_quark = g_quark_from_static_string("gsthandleerrors:override"););

typedef struct
{
    GstHandleErrors *self; /* borrowed */
    GstPadChainFunction orig_chain_func;
    GstPadChainListFunction orig_chain_list_func;
} PadOverride;

static gboolean
gst_handle_errors_post_message(GstElement *element, GstMessage *msg)
{
    if (GST_MESSAGE_TYPE(msg) == GST_MESSAGE_ERROR)
    {
        GError *err = NULL;
        gchar *dbg_info = NULL;
        GstStructure *s;

        gst_message_parse_error(msg, &err, &dbg_info);
        GST_DEBUG_OBJECT(element, "Received error: %s (%s)", err->message, dbg_info);
        gst_message_unref(msg);

        s = gst_structure_new("x-handle-error:message-error", "error-domain", G_TYPE_UINT, err->domain,
                              "error-code", G_TYPE_UINT, err->code,
                              "error-message", G_TYPE_STRING, err->message,
                              "debug", G_TYPE_STRING, dbg_info,
                              NULL);

        msg = gst_message_new_custom(GST_MESSAGE_ELEMENT, GST_OBJECT_CAST(element), s);

        g_error_free(err);
        g_free(dbg_info);
    }

    return GST_ELEMENT_CLASS(parent_class)->post_message(element, msg);
}

static PadOverride *
pad_override_new(GstHandleErrors *self, GstPad *pad)
{
    PadOverride *o = g_new0(PadOverride, 1);

    o->self = self;
    o->orig_chain_func = GST_PAD_CHAINFUNC(pad);
    o->orig_chain_list_func = GST_PAD_CHAINLISTFUNC(pad);

    return o;
}

static void
pad_override_free(PadOverride *o)
{
    g_free(o);
}

static GstFlowReturn
check_ret(GstPad *pad, GstObject *parent, PadOverride *o, GstFlowReturn ret)
{
    gchar *pad_name, *elt_name;
    GstStructure *s;
    GstMessage *msg;

    if (ret != GST_FLOW_ERROR)
        return ret;

    pad_name = gst_pad_get_name(pad);
    elt_name = gst_element_get_name(GST_ELEMENT(parent));

    GST_DEBUG_OBJECT(o->self, "Chained failed on pad %s:%s return OK instead", elt_name, pad_name);

    s = gst_structure_new("x-handle-error:chain-error",
                          "element-name", G_TYPE_STRING, elt_name,
                          "pad-name", G_TYPE_STRING, pad_name,
                          NULL);

    g_free(pad_name);
    g_free(elt_name);

    msg = gst_message_new_custom(GST_MESSAGE_ELEMENT, GST_OBJECT_CAST(o->self), s);
    gst_element_post_message(GST_ELEMENT(parent), msg);

    return GST_FLOW_OK;
}

static GstFlowReturn
gst_handle_errors_chain_func(GstPad *pad, GstObject *parent,
                             GstBuffer *buffer)
{
    GstFlowReturn ret;
    PadOverride *o;

    o = g_object_get_qdata((GObject *)pad, override_quark);
    g_assert(o);

    ret = o->orig_chain_func(pad, parent, buffer);

    return check_ret(pad, parent, o, ret);
}

static GstFlowReturn
gst_handle_errors_chain_list_func(GstPad *pad, GstObject *parent,
                                  GstBufferList *list)
{
    GstFlowReturn ret;
    PadOverride *o;

    o = g_object_get_qdata((GObject *)pad, override_quark);
    g_assert(o);

    ret = o->orig_chain_list_func(pad, parent, list);

    return check_ret(pad, parent, o, ret);
}

static void
setup_override_on_pad(GstHandleErrors *self, GstPad *pad)
{
    if (GST_PAD_CHAINFUNC(pad) || GST_PAD_CHAINLISTFUNC(pad))
    {
        PadOverride *o = pad_override_new(self, pad);

        g_object_set_qdata_full((GObject *)pad, override_quark, o, (GDestroyNotify)pad_override_free);

        gst_pad_set_chain_function(pad, gst_handle_errors_chain_func);
        gst_pad_set_chain_list_function(pad, gst_handle_errors_chain_list_func);
    }
}

static gboolean
override_sink_pad_foreach(GstElement *element,
                          GstPad *pad,
                          gpointer user_data)
{
    GstHandleErrors *self = user_data;

    setup_override_on_pad(self, pad);
    return TRUE;
}

static void
pad_added_cb(GstElement *element, GstPad *pad, GstHandleErrors *self)
{
    setup_override_on_pad(self, pad);
}

typedef gboolean (*ElementForeachPadFunc)(GstElement *element,
                                          GstPad *pad,
                                          gpointer user_data);
static gboolean
element_foreach_sink_pad(GstElement *element, ElementForeachPadFunc func, gpointer user_data)
{
    GList *l;
    gboolean res = FALSE;

    GST_OBJECT_LOCK(element);
    for (l = element->sinkpads; l; l = g_list_next(l))
    {
        GstPad *pad = (GstPad *)l->data;

        res = func(element, pad, user_data);
        if (!res)
            goto done;
    }

done:
    GST_OBJECT_UNLOCK(element);
    return res;
}

static gboolean
gst_handle_errors_add_element(GstBin *bin, GstElement *element)
{
    GstHandleErrors *self = GST_HANDLE_ERRORS(bin);

    element_foreach_sink_pad(element, override_sink_pad_foreach, self);
    g_signal_connect_object(element, "pad-added", G_CALLBACK(pad_added_cb), self, 0);

    return GST_BIN_CLASS(parent_class)->add_element(bin, element);
}

static gboolean
remove_override_sink_pad_foreach(GstElement *element,
                                 GstPad *pad,
                                 gpointer user_data)
{
    PadOverride *o;

    o = g_object_get_qdata((GObject *)pad, override_quark);
    g_assert(o);

    gst_pad_set_chain_function(pad, o->orig_chain_func);
    gst_pad_set_chain_list_function(pad, o->orig_chain_list_func);

    g_object_set_qdata((GObject *)pad, override_quark, NULL);

    return TRUE;
}

static gboolean
gst_handle_errors_remove_element(GstBin *bin, GstElement *element)
{
    GstHandleErrors *self = GST_HANDLE_ERRORS(bin);

    element_foreach_sink_pad(element, remove_override_sink_pad_foreach, self);
    g_signal_handlers_disconnect_by_func(element, pad_added_cb, self);

    return GST_BIN_CLASS(parent_class)->remove_element(bin, element);
}

static void
gst_handle_errors_class_init(GstHandleErrorsClass *klass)
{
    GstElementClass *element_class = GST_ELEMENT_CLASS(klass);
    GstBinClass *bin_class = GST_BIN_CLASS(klass);

    gst_element_class_set_static_metadata(element_class,
                                          "Handle Errors", "Bin",
                                          "Bin catching errors from its elements and posting a custom message instead",
                                          "Guillaume Desmottes <guillaume.desmottes@collabora.com>");

    element_class->post_message = gst_handle_errors_post_message;

    bin_class->add_element = gst_handle_errors_add_element;
    bin_class->remove_element = gst_handle_errors_remove_element;
}

static void
gst_handle_errors_init(GstHandleErrors *self)
{
}